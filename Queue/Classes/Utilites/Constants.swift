//
//  Constants.swift
//  Queue
//
//  Created by Vivek Padaya on 02/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class Constants: NSObject {
    static func UIColorFromRGB(r:Float,g:Float,b:Float) -> UIColor {
        return UIColor(
            red: CGFloat(r) / 255.0,
            green: CGFloat(g) / 255.0,
            blue: CGFloat(b) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    static var defaultColor = UIColorFromRGB(r: 251, g: 187, b: 1)
    
    
    static func defaultFont(size: Int) -> UIFont{
        return UIFont(name: "Roboto-Regular", size: CGFloat(size))!
    }
    
    static func defaultBoldFont(size: Int) -> UIFont{
        return UIFont(name: "Roboto-Bold", size: CGFloat(size))!
    }

}
