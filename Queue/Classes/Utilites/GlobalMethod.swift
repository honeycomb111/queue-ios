//
//  GlobalMethod.swift
//  Queue
//
//  Created by Vivek Padaya on 02/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import GameplayKit

class GlobalMethod: NSObject {

}

@IBDesignable
extension UITextField {
    
    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }
    
    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

extension UIView {
    
    func setBoarderGradiantWithColor(startColor : UIColor, endColor : UIColor){
        self.layer.cornerRadius = 10.0
        self.clipsToBounds = true
        
        
        let gradientNew = CAGradientLayer()
        gradientNew.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradientNew.colors = [startColor.cgColor, endColor.cgColor]
        gradientNew.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientNew.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let shapeNew = CAShapeLayer()
        shapeNew.lineWidth = 2
        shapeNew.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10.0).cgPath
        shapeNew.strokeColor = UIColor.black.cgColor
        shapeNew.fillColor = UIColor.clear.cgColor
        
        gradientNew.mask = shapeNew
        
        self.layer.addSublayer(gradientNew)
    }
    
}

extension UIViewController {
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    func isValidEmail(testStr:String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17)
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 12)
        two.textAlignment = .center
        two.sizeToFit()
        
        
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        
        
        
        self.navigationItem.titleView = stackView
    }
}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    func toFloat() -> Float? {
        if self.isEmpty{
            return 0.0
        }
        return NumberFormatter().number(from: self)?.floatValue
    }
    
    func toInteger() -> Int? {
        return NumberFormatter().number(from: self)?.intValue
    }
}

extension Float {
    func round(decimalPlace:Int)->Float{
        let format = NSString(format: "%%.%if", decimalPlace)
        let string = NSString(format: format, self)
        return Float(atof(string.utf8String))
    }
}


func generateUniqueString() -> String {
    return NSUUID().uuidString
}

func randomString(length : Int) -> String {
    let charSet = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZ".characters)
    let shuffled = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: charSet) as! [Character]
    let array = shuffled.prefix(length)
    return String(array)
}

extension Date {
    
    // Year
    var currentYear: String? {
        return getDateComponent(dateFormat: "yyyy")
        //return getDateComponent(dateFormat: "yyyy")
    }
    
    
    func getDateComponent(dateFormat: String) -> String? {
        let format = DateFormatter()
        format.dateFormat = dateFormat
        return format.string(from: self)
    }
    
    
}

extension Int
{
    func toString() -> String
    {
        let myString = String(self)
        return myString
    }
}

// Helper function inserted by Swift 4.2 migrator.
func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}



extension UIProgressView {
    
    @IBInspectable var barHeight : CGFloat {
        get {
            return transform.d * 2.0
        }
        set {
            // 2.0 Refers to the default height of 2
            let heightScale = newValue / 2.0
            let c = center
            transform = CGAffineTransform(scaleX: 1.0, y: heightScale)
            center = c
        }
    }
}
