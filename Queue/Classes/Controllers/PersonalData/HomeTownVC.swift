//
//  HomeTownVC.swift
//  Queue
//
//  Created by Vivek Padaya on 06/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class HomeTownVC: UIViewController, UITextFieldDelegate {
    var personalTabvc : PersonalDataTabVC?

    @IBOutlet weak var viewHomeTown: UIView!
    @IBOutlet weak var txtHometown: UITextField!
    @IBOutlet weak var viewNext: UIView!
    
    
    var userModel : UserModel = UserModel.init()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtHometown.delegate = self
        
        self.userModel = self.personalTabvc?.userModel ?? UserModel.init()
        
        // Do any additional setup after loading the view.
        self.viewHomeTown.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
    }
    
    @IBAction func actnNext(_ sender: Any) {
        
//        userModel.height = txtHeight.text ?? ""
        
        self.personalTabvc?.userModel = userModel
        self.personalTabvc?.tabScrollView.changePageToIndex(5, animated: true)
    }
    
   

}
