//
//  HeightVC.swift
//  Queue
//
//  Created by Vivek Padaya on 06/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import ActionSheetPicker

class HeightVC: UIViewController, UITextFieldDelegate {
    var personalTabvc : PersonalDataTabVC?

    @IBOutlet weak var viewHeight: UIView!
    @IBOutlet weak var viewNext: UIView!
    @IBOutlet weak var txtHeight: UITextField!
    
    var userModel : UserModel = UserModel.init()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtHeight.delegate = self
        
        self.userModel = self.personalTabvc?.userModel ?? UserModel.init()
        
        // Do any additional setup after loading the view.
        self.viewHeight.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        // Do any additional setup after loading the view.
        
        
    }
    
    @IBAction func actnNext(_ sender: Any) {
        userModel.height = txtHeight.text ?? ""
        
        self.personalTabvc?.userModel = userModel
        self.personalTabvc?.tabScrollView.changePageToIndex(4, animated: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.selectHeight()
        return false
    }
    
    func selectHeight(){
        
        var arrayStr = [String]()
        
        for i in (4..<8){
            for j in (1..<12){
                let string = "\(i)'\(j)\""
                arrayStr.append(string)
            }
        }
        
        
        ActionSheetStringPicker.show(withTitle: "Select Heoght", rows: arrayStr, initialSelection: 0, doneBlock: { (picker, index, values) in
            print("Selected \(values)")
            self.txtHeight.text = values as! String
            
        }, cancel: nil, origin: self.view)
        
      
    }
}
