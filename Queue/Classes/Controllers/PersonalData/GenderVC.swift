//
//  GenderVC.swift
//  Queue
//
//  Created by Vivek Padaya on 05/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class GenderVC: UIViewController, UITextFieldDelegate {
    var personalTabvc : PersonalDataTabVC?
    var objectiveTabvc : FilterTypeTabVC?


    @IBOutlet weak var viewNext: UIView!
    @IBOutlet weak var viewGender: UIView!
    
    @IBOutlet weak var txtGender: UITextField!
    
    var userModel : UserModel = UserModel.init()
    
    var isFromFilter = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.personalTabvc != nil {
            self.userModel = self.personalTabvc?.userModel ?? UserModel.init()
        }
        
        // Do any additional setup after loading the view.
        self.viewGender.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        // Do any additional setup after loading the view.
        self.txtGender.delegate = self
        
        
    }
    
    func selectedGender(gender : String){
        self.txtGender.text = gender
    }
    

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let genderPop = self.storyboard?.instantiateViewController(withIdentifier: "GenderPopVC") as! GenderPopVC
        genderPop.delegate = self
        genderPop.modalPresentationStyle = .overCurrentContext
        genderPop.modalTransitionStyle = .crossDissolve

        self.present(genderPop, animated: true, completion: nil)
        
        return false
    }
  
    @IBAction func actnNext(_ sender: Any) {
        
        
        if isFromFilter {
            objectiveTabvc?.tabScrollView.changePageToIndex(1, animated: true)
        }else{
            userModel.gender = txtGender.text ?? ""
            
            self.personalTabvc?.userModel = userModel
            self.personalTabvc?.tabScrollView.changePageToIndex(3, animated: true)
        }
    }
    
}
