//
//  CurrentLocationVC.swift
//  Queue
//
//  Created by Vivek Padaya on 06/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class CurrentLocationVC: UIViewController, UITextFieldDelegate {
    var personalTabvc : PersonalDataTabVC?

    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var txtCurrentLocation: UITextField!
    
    @IBOutlet weak var viewNext: UIView!
    var userModel : UserModel = UserModel.init()
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        txtCurrentLocation.delegate = self
        
        self.userModel = self.personalTabvc?.userModel ?? UserModel.init()
        
        // Do any additional setup after loading the view.
        self.viewLocation.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
    }
    
    @IBAction func actnNext(_ sender: Any) {
//        let tab = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
//        self.navigationController?.pushViewController(tab, animated: true)
        
        let head = self.storyboard?.instantiateViewController(withIdentifier: "HeadPicVC") as! HeadPicVC
        self.navigationController?.pushViewController(head, animated: true)
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let locPop = self.storyboard?.instantiateViewController(withIdentifier: "LocationPopVC") as! LocationPopVC
        locPop.delegate = self
        locPop.modalPresentationStyle = .overCurrentContext
        locPop.modalTransitionStyle = .crossDissolve
        
        self.present(locPop, animated: true, completion: nil)
        
        return false
    }
   

}
