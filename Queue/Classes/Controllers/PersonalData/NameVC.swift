//
//  NameVC.swift
//  Queue
//
//  Created by Vivek Padaya on 05/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class NameVC: UIViewController {

    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewNext: UIView!
    
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    
    var personalTabvc : PersonalDataTabVC?
    
    let userModel : UserModel = UserModel.init()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setGradiant()
    }
    
    func setGradiant(){
        self.viewFirstName.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewLastName.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        
    }
    
    @IBAction func actnNext(_ sender: Any) {
        
        userModel.firstName = txtFName.text ?? ""
        userModel.lastName = txtLName.text ?? ""
        
        self.personalTabvc?.userModel = userModel
        self.personalTabvc?.tabScrollView.changePageToIndex(1, animated: true)
    }
    
}
