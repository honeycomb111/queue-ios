//
//  BirthdayVC.swift
//  Queue
//
//  Created by Vivek Padaya on 05/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import ActionSheetPicker

class BirthdayVC: UIViewController, UITextFieldDelegate {

    var personalTabvc : PersonalDataTabVC?
    @IBOutlet weak var viewBirthday: UIView!
    @IBOutlet weak var viewNext: UIView!
    @IBOutlet weak var txtBirthday: UITextField!
    
    var userModel : UserModel = UserModel.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userModel = self.personalTabvc?.userModel ?? UserModel.init()

        // Do any additional setup after loading the view.
        self.viewBirthday.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        
        self.txtBirthday.delegate = self
        
    }
    
 
    @IBAction func actnNext(_ sender: Any) {
      
        
        self.userModel.birthDay = self.txtBirthday.text ?? ""
        self.personalTabvc?.userModel = userModel
        self.personalTabvc?.tabScrollView.changePageToIndex(2, animated: true)

    }
    
    
    func datePickerTapped() {
        
        
        let datePicker = ActionSheetDatePicker(title: "Select Birthdate", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            
            if let dt = value {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                self.txtBirthday.text = formatter.string(from: dt as! Date)
            }
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
     
        datePicker?.maximumDate = Date()
        datePicker?.show()
        
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        self.datePickerTapped()
        return false
    }
}
