
//
//  GenderPopVC.swift
//  Queue
//
//  Created by Vivek Padaya on 06/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class GenderPopVC: UIViewController {

    @IBOutlet weak var viewBackground: UIView!
    var delegate : GenderVC?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapOnBack : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(GenderPopVC.dismissView))
        self.viewBackground.addGestureRecognizer(tapOnBack)
        
        
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actnFemale(_ sender: Any) {
        self.delegate?.selectedGender(gender: "Female")
        self.dismissView()
    }
    
    @IBAction func actnMale(_ sender: Any) {
        self.delegate?.selectedGender(gender: "Male")
        self.dismissView()
    }
    
}
