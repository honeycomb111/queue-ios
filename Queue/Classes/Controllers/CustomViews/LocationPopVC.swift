//
//  LocationPopVC.swift
//  Queue
//
//  Created by Vivek Padaya on 09/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class LocationPopVC: UIViewController {

    var delegate : CurrentLocationVC?

    @IBOutlet weak var viewbackground: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tapOnBack : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(GenderPopVC.dismissView))
        self.viewbackground.addGestureRecognizer(tapOnBack)
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actnAutomatically(_ sender: Any) {
        self.dismissView()

    }
    
    
    @IBAction func actnManually(_ sender: Any) {
        self.dismissView()

    }
    

}
