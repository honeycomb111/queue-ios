//
//  ProfessionVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class ProfessionVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var viewProfession: UIView!
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var viewNext: UIView!
    
    @IBOutlet weak var txtProfession: UITextField!
    @IBOutlet weak var txtCompany: UITextField!
    
    var professionalTabvc : ProfessionalTabBar?
    
    var userModel : UserModel = UserModel.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.userModel = self.professionalTabvc?.userModel ?? UserModel.init()
        txtProfession.delegate = self
        setGradiant()
    }
    
    func setGradiant(){
        self.viewProfession.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewCompany.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let quaPop = self.storyboard?.instantiateViewController(withIdentifier: "ProfessionPopVC") as! ProfessionPopVC
        quaPop.delegate = self
        quaPop.modalPresentationStyle = .overCurrentContext
        quaPop.modalTransitionStyle = .crossDissolve
        
        self.present(quaPop, animated: true, completion: nil)
        
        return false
    }
    
    @IBAction func actnNext(_ sender: Any) {
        
        userModel.profession = txtProfession.text ?? ""
        userModel.company = txtCompany.text ?? ""
        
        let lifeStyle = self.storyboard?.instantiateViewController(withIdentifier: "LifestyleTabBar") as! LifestyleTabBar
        lifeStyle.userModel = userModel
        self.navigationController?.pushViewController(lifeStyle, animated: true)
    }
   
    func professionTag(tag: Int){
        
    }
}
