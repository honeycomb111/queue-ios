//
//  QualificationVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class QualificationVC: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var viewQualification: UIView!
    @IBOutlet weak var viewInstitue: UIView!
    @IBOutlet weak var viewNext: UIView!
    
    @IBOutlet weak var txtQualification: UITextField!
    @IBOutlet weak var txtInstitute: UITextField!
    
    var professionalTabvc : ProfessionalTabBar?
    
    var userModel : UserModel = UserModel.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.userModel = self.professionalTabvc?.userModel ?? UserModel.init()
        setGradiant()
        txtQualification.delegate = self
    }
    

    func setGradiant(){
        self.viewQualification.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewInstitue.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let quaPop = self.storyboard?.instantiateViewController(withIdentifier: "QualificationPopVC") as! QualificationPopVC
        quaPop.delegate = self
        quaPop.modalPresentationStyle = .overCurrentContext
        quaPop.modalTransitionStyle = .crossDissolve
        
        self.present(quaPop, animated: true, completion: nil)
        
        return false
    }
    
    
    @IBAction func actnNext(_ sender: Any) {
        
        userModel.qualification = txtQualification.text ?? ""
        userModel.institute = txtInstitute.text ?? ""
        
        self.professionalTabvc?.userModel = userModel
        self.professionalTabvc?.tabScrollView.changePageToIndex(1, animated: true)
    }
    
    func qualificationTag(tag: Int){
        
    }

}
