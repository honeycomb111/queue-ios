//
//  QualificationPopVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class QualificationPopVC: UIViewController {

    @IBOutlet weak var viewBackground: UIView!
    var delegate : QualificationVC?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tapOnBack : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(QualificationPopVC.dismissView))
        self.viewBackground.addGestureRecognizer(tapOnBack)
        
        
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func actnQualification(_ sender: UIButton) {
    
    }
}
