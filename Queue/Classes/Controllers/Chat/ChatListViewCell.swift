//
//  ChatListViewCell.swift
//  DeliverLoad
//
//  Created by Vivek Padaya on 30/08/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

import SFProgressCircle

class ChatListViewCell: UITableViewCell {

    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    
    
    @IBOutlet var lblRef: UILabel!
   
    @IBOutlet weak var viewAvatar: UIView!
    @IBOutlet weak var avtarImg: UIImageView!
    
    var progressBarGradiant : SFCircleGradientView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpProgress()
    }
    
    func setUpProgress(){
        
        progressBarGradiant = SFCircleGradientView.init(frame: viewAvatar.bounds)
        progressBarGradiant?.lineWidth = 4
        progressBarGradiant?.progress = 0.7
        
        progressBarGradiant?.startColor = Constants.UIColorFromRGB(r: 255, g: 67, b: 91)
        progressBarGradiant?.endColor = Constants.UIColorFromRGB(r: 38, g: 108, b: 237)
        self.viewAvatar.addSubview(progressBarGradiant!)
        self.viewAvatar.sendSubviewToBack(progressBarGradiant!)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
