//
//  ChatListVC.swift
//  Queue
//
//  Created by Vivek Padaya on 14/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ActiveMsg {
    var name = ""
    var fromUserId = 0
    var userImgName = ""
    var totalUnreadCount = 0
    var loadId = 0
    
    required init() {
        print("init model")
    }
    
}

class ChatListVC: BaseVC {
    
    @IBOutlet var tableView: UITableView!
    
    var timer : Timer?
    
    var msgListArray = [ActiveMsg]()
    var lastSelectedMsg : ActiveMsg?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tableView.register(UINib(nibName: "ChatListViewCell", bundle: nil), forCellReuseIdentifier: "ChatListViewCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
       
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getActiveChat()
        timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(getActiveChat), userInfo: nil, repeats: true)
        self.lastSelectedMsg = nil
        
       setUpNavigation()
    }
    
    
    func setUpNavigation(){
        self.setTitle(title: "The Queue", subtitle: "")
        //        self.titleString = "Messages"
        self.navigationItem.rightBarButtonItem = nil

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "msg_nav_img"),
                                                                    for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        let leftBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        leftBtn.setBackgroundImage(UIImage(named: "ic_search"), for: .normal)//(UIImage(named: "ic_search"), for: .normal)
        leftBtn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

        
        let viewImage = UIView.init(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let imageIcon = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageIcon.image = UIImage(named: "ic_search")
        imageIcon.contentMode = .scaleAspectFit
        imageIcon.clipsToBounds = true
        viewImage.addSubview(imageIcon)
        viewImage.clipsToBounds = true
        
        let leftBarButton = UIBarButtonItem.init(customView: viewImage)
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        
        let rightBtn = UIButton.init(frame: CGRect(x: 0, y: -5, width: 80, height: 25))
        rightBtn.setImage(UIImage(named: "ic_forward"), for: .normal)
        rightBtn.setTitle("Archived", for: .normal)
        rightBtn.setTitleColor(UIColor.black, for: .normal)
        rightBtn.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        
        rightBtn.addTarget(self, action: #selector(actnAcrhived), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem.init(customView: rightBtn)
        
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    
    deinit {
        timer = nil
    }
    
    @objc func actnAcrhived(){
        let archived = self.storyboard?.instantiateViewController(withIdentifier: "ArchivedLIstVC") as! ArchivedLIstVC
        self.navigationController?.pushViewController(archived, animated: true)
    }
    
    @objc func getActiveChat(){
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ChatListVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let msg = msgListArray[indexPath.row]
        
//        let user = UserManager.loginUser!
        let chat = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        chat.senderDisplayName = "Test"//user.firstName + user.lastName
        chat.senderId = "Test"//user.userId.toString()
//        chat.activeMsg = msg
//        lastSelectedMsg = msg
        self.navigationController?.pushViewController(chat, animated: true)
    }
}

extension ChatListVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListViewCell", for: indexPath) as! ChatListViewCell
        
//        let msg = msgListArray[indexPath.row]
//
//        cell.lblName.text = msg.name
//        cell.lblRef.text = "Ref : \(msg.loadId)"
        
//        if msg.userImgName.count > 0{
//            let imgUrl = Constants.baseUrlProfilePic + msg.userImgName
//            cell.imgProfile.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "profile_bg"), options: .refreshCached, completed: nil)
//            cell.imgProfile.contentMode = .scaleAspectFill
//        }
//        
       
        return cell
    }
}

