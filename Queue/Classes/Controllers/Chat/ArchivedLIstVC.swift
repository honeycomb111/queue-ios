//
//  ArchivedLIstVC.swift
//  Queue
//
//  Created by Vivek Padaya on 18/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class ArchivedLIstVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var timer : Timer?
    
    var msgListArray = [ActiveMsg]()
    var lastSelectedMsg : ActiveMsg?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.register(UINib(nibName: "ChatListViewCell", bundle: nil), forCellReuseIdentifier: "ChatListViewCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        
        
        self.setTitle(title: "Archived", subtitle: "")
    }
    

    

}

extension ArchivedLIstVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let msg = msgListArray[indexPath.row]
        
        //        let user = UserManager.loginUser!
        let chat = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        chat.senderDisplayName = "Test"//user.firstName + user.lastName
        chat.senderId = "Test"//user.userId.toString()
        //        chat.activeMsg = msg
        //        lastSelectedMsg = msg
        self.navigationController?.pushViewController(chat, animated: true)
    }
}

extension ArchivedLIstVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListViewCell", for: indexPath) as! ChatListViewCell
        
        //        let msg = msgListArray[indexPath.row]
        //
        //        cell.lblName.text = msg.name
        //        cell.lblRef.text = "Ref : \(msg.loadId)"
        
        //        if msg.userImgName.count > 0{
        //            let imgUrl = Constants.baseUrlProfilePic + msg.userImgName
        //            cell.imgProfile.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "profile_bg"), options: .refreshCached, completed: nil)
        //            cell.imgProfile.contentMode = .scaleAspectFill
        //        }
        //
        
        return cell
    }
}


