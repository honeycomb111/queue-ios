//
//  ChatVC.swift
//  Queue
//
//  Created by Vivek Padaya on 14/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SVProgressHUD
import AudioToolbox
import AVFoundation

class ChatVC: JSQMessagesViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    private var messages: [JSQMessage] = []
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    var activeMsg : ActiveMsg? = nil
    
    var timer : Timer?
    var imagePicker = UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.collectionView.backgroundColor = UIColor.groupTableViewBackground
        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        getMessages()
        setUpNav()
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(refreshMessage), userInfo: nil, repeats: true)
//        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        let rightButton = UIButton()
        let sendImage = UIImage(named: "ic_send")
        rightButton.setImage(sendImage, for: UIControl.State.normal)
        
        self.inputToolbar.contentView.rightBarButtonItemWidth = CGFloat(34.0)
        
        self.inputToolbar.contentView.rightBarButtonItem = rightButton
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.setTitle(title: "Joe Kelly", subtitle: "")
        
        
        
        
        let viewImage = UIView.init(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let imageIcon = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 25))
        imageIcon.image = UIImage(named: "ic_more")
        imageIcon.contentMode = .scaleAspectFit
        imageIcon.clipsToBounds = true
        viewImage.addSubview(imageIcon)
        viewImage.clipsToBounds = true

        let btnRight = UIButton.init(frame: viewImage.bounds)
        btnRight.addTarget(self, action: #selector(actnMore), for: .touchUpInside)
        viewImage.addSubview(btnRight)

        let rightBarButton = UIBarButtonItem.init(customView: viewImage)
    


        self.navigationItem.rightBarButtonItem = rightBarButton
//        self.navigationController?.view.backgroundColor = .white
//        self.navigationController?.navigationBar.tintColor = UIColor.white

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        timer = nil
    }
    deinit {
        timer = nil
    }
    
    @objc func actnMore(){
        print("pressed more")
        let actionPop = self.storyboard?.instantiateViewController(withIdentifier: "ActionPopVC") as! ActionPopVC
        actionPop.modalPresentationStyle = .overCurrentContext
        actionPop.modalTransitionStyle = .crossDissolve
        
        self.present(actionPop, animated: true, completion: nil)

    }
    
    func setUpNav(){
        let titleFrame = CGRect(x: 0, y: 0, width: self.view.frame.width/2, height: 30)
        let customViewTitle = UIView(frame: titleFrame)
        let titleLabel = UILabel(frame: titleFrame)
        titleLabel.text = self.activeMsg?.name
        titleLabel.font = Constants.defaultBoldFont(size: 15)
        titleLabel.textAlignment = .center
        customViewTitle.addSubview(titleLabel)
        self.navigationItem.titleView = customViewTitle
        
        let customView = UIView.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let iconView = UIImageView.init(frame: CGRect(x: 10, y: 5, width: 30, height: 30))
        iconView.image = UIImage(named: "logo_small")
        iconView.contentMode = .scaleAspectFit
        iconView.clipsToBounds = true
        customView.addSubview(iconView)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: customView)
    }
    
    func getMessages(){
//        let currentUser = UserManager.loginUser!
        
//        var params : Parameters = ["MSG_FromUserId" : activeMsg!.fromUserId.toString(),
//                                   "MSG_ToUserId" : currentUser.userId.toString(),//activeMsg!.fromUserId.toString(),
//            "MSG_LoadId" : activeMsg?.loadId]
//        //        var params : Parameters = ["MSG_FromUserId" : currentUser.userId.toString(),
//        //                                   "MSG_ToUserId" : activeMsg!.fromUserId.toString(),
//        //                                   "MSG_LoadId" : 1]
//
//        if (activeMsg?.totalUnreadCount)! > 0 {
//            params["IsReceiver"] = "true"
//        }
//        APIManager.makeRequestWithMethod(methodName: "chat/getchatmessage", HttpMethod: .post, params: params) { (response) in
//
//            if let responseDict = response.result as? NSDictionary{
//                if let msgData = responseDict.object(forKey: "Data") as? NSDictionary {
//
//                    if let msgList = msgData.object(forKey: "ChatMesssage") as? [[String : AnyObject]]{
//                        for msg in msgList{
//                            let fromId = msg["MSG_FromUserId"] as! Int
//                            let fromName = self.activeMsg?.name as! String
//                            if let text = msg["MSG_Message"] as? String {
//                                self.addMessage(withId: "\(fromId)", name: fromName, text: text)
//                            }
//                        }
//                    }
//                    self.finishReceivingMessage()
//
//                }else{
//                    let msg = responseDict.object(forKey: "Message") as! String
//                    self.alert(title: "", message: msg)
//                }
//            }
//
//        }
        
    }
    
    @objc func refreshMessage(){
//        let currentUser = UserManager.loginUser!
//
//        var params : Parameters = ["MSG_FromUserId" : activeMsg!.fromUserId.toString(),
//                                   "MSG_ToUserId" : currentUser.userId.toString(),//activeMsg!.fromUserId.toString(),
//            "MSG_LoadId" : activeMsg?.loadId]
//
//        if (activeMsg?.totalUnreadCount)! > 0 {
//            params["IsReceiver"] = "true"
//        }
//        APIManager.makeRequestWithMethod(methodName: "chat/getchatmessage", HttpMethod: .post, params: params) { (response) in
//
//            if let responseDict = response.result as? NSDictionary{
//                if let msgData = responseDict.object(forKey: "Data") as? NSDictionary {
//
//                    if let msgList = msgData.object(forKey: "ChatMesssage") as? [[String : AnyObject]]{
//                        if self.messages.count < msgList.count {
//                            self.messages.removeAll()
//                            for msg in msgList{
//
//                                let fromId = msg["MSG_FromUserId"] as! Int
//                                let fromName = self.activeMsg?.name as! String
//                                if let text = msg["MSG_Message"] as? String {
//                                    self.addMessage(withId: "\(fromId)", name: fromName, text: text)
//                                }
//                            }
//                        }
//                    }
//                    self.finishReceivingMessage()
//
//                }else{
//                    let msg = responseDict.object(forKey: "Message") as! String
//                    self.alert(title: "", message: msg)
//                }
//            }
//
//        }
    }
    
    // MARK: Collection view data source (and related) methods
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        
        if message.senderId == senderId { // 1
            cell.textView?.textColor = UIColor.white // 2
        } else {
            cell.textView?.textColor = UIColor.black // 3
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString? {
    //        let message = messages[indexPath.item]
    //        switch message.senderId {
    //        case senderId:
    //            return nil
    //        default:
    //            guard let senderDisplayName = message.senderDisplayName else {
    //                assertionFailure()
    //                return nil
    //            }
    //            return NSAttributedString(string: senderDisplayName)
    //        }
    //    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        
        self.addMessage(withId: "1", name: "test", text: text)

        self.finishSendingMessage()
//
//        let user = UserManager.loginUser!
//        let aMsg = activeMsg!
//
//        if let textMsg = text, textMsg.count > 0 {
//            let params : Parameters = ["MSG_FromUserId" : user.userId.toString(),
//                                       "MSG_ToUserId" : aMsg.fromUserId.toString(),
//                                       "MSG_Message" : textMsg,
//                                       "MSG_LoadId" : aMsg.loadId]
//            self.inputToolbar.contentView.textView.text = ""
//
//            APIManager.makeRequestWithMethod(methodName: "chat/sendmessage", HttpMethod: .post, params: params) { (response) in
//                if let responseDict = response.result as? NSDictionary{
//                    if let msgData = responseDict.object(forKey: "Data") as? NSDictionary {
//                        self.addMessage(withId: user.userId.toString(), name: self.senderDisplayName, text: text)
//                        self.finishSendingMessage()
//                    }else{
//                        let msg = responseDict.object(forKey: "Message") as! String
//                        self.alert(title: "", message: msg)
//                    }
//                }
//            }
//        }
        
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        print("Camera button")
        clickOnImage()
    }
    
    
    // MARK: UI and User Interaction
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            messages.append(message)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func clickOnImage(){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
//        switch UIDevice.current.userInterfaceIdiom {
//        case .pad:
////            alert.popoverPresentationController?.sourceView = self.viewMain
////            alert.popoverPresentationController?.sourceRect = self.viewMain.bounds
////            alert.popoverPresentationController?.permittedArrowDirections = .up
//        default:
//            break
//        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
    if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    //MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            let mediaItem = JSQPhotoMediaItem(image: nil)
            mediaItem?.appliesMediaViewMaskAsOutgoing = true
            mediaItem?.image = pickedImage
            let sendMessage = JSQMessage(senderId: senderId, displayName: self.senderDisplayName, media: mediaItem)
            self.messages.append(sendMessage!)
            self.finishSendingMessage()
            
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}
