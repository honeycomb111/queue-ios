
//
//  MembershipListVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

enum MemberPlan : Int {
    case free = 0
    case genral = 1
    case guest = 2
    case vip = 3
}

class MembershipListVC: UIViewController {

    @IBOutlet weak var viewFirst: UIView!
    @IBOutlet weak var viewSecond: UIView!
    @IBOutlet weak var viewThird: UIView!
    @IBOutlet weak var viewFourth: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.navigationController?.navigationBar.isHidden = true
        addGesture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func addGesture(){
        
        let firstGesture = UITapGestureRecognizer.init(target: self, action: #selector(actnFirst))
        self.viewFirst.addGestureRecognizer(firstGesture)
        
        let secondGesture = UITapGestureRecognizer.init(target: self, action: #selector(actnSecond))
        self.viewSecond.addGestureRecognizer(secondGesture)
        
        let thirdGesture = UITapGestureRecognizer.init(target: self, action: #selector(actnThird))
        self.viewThird.addGestureRecognizer(thirdGesture)
        
        let fourtGesture = UITapGestureRecognizer.init(target: self, action: #selector(actnFourth))
        self.viewFourth.addGestureRecognizer(fourtGesture)
    }
    
    @objc func actnFirst(){
        let professional = self.storyboard?.instantiateViewController(withIdentifier: "ProfessionalTabBar") as! ProfessionalTabBar
        self.navigationController?.pushViewController(professional, animated: true)
    }
    
    @objc func actnSecond(){
        
    }
    
    @objc func actnThird(){
        
    }
    
    @objc func actnFourth(){
        
    }
    
    
    @IBAction func actnInfoFirst(_ sender: Any) {
        let info = self.storyboard?.instantiateViewController(withIdentifier: "MemberInfoPopUpVC") as! MemberInfoPopUpVC
        info.memberPlan = MemberPlan.free
        info.modalPresentationStyle = .overCurrentContext
        info.modalTransitionStyle = .crossDissolve
        
        self.present(info, animated: true, completion: nil)
    }
    
    @IBAction func actnInfoSecond(_ sender: Any) {
        let info = self.storyboard?.instantiateViewController(withIdentifier: "MemberInfoPopUpVC") as! MemberInfoPopUpVC
        info.memberPlan = MemberPlan.genral
        info.modalPresentationStyle = .overCurrentContext
        info.modalTransitionStyle = .crossDissolve
        
        self.present(info, animated: true, completion: nil)
    }
    
    @IBAction func actnInfoThird(_ sender: Any) {
        let info = self.storyboard?.instantiateViewController(withIdentifier: "MemberInfoPopUpVC") as! MemberInfoPopUpVC
        info.memberPlan = MemberPlan.guest
        info.modalPresentationStyle = .overCurrentContext
        info.modalTransitionStyle = .crossDissolve
        
        self.present(info, animated: true, completion: nil)
    }
    
    @IBAction func actnInfoFourth(_ sender: Any) {
        let info = self.storyboard?.instantiateViewController(withIdentifier: "MemberInfoPopUpVC") as! MemberInfoPopUpVC
        info.memberPlan = MemberPlan.vip
        info.modalPresentationStyle = .overCurrentContext
        info.modalTransitionStyle = .crossDissolve
        
        self.present(info, animated: true, completion: nil)
    }
}
