//
//  MemberInfoPopUpVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class MemberInfoPopUpVC: UIViewController {

    @IBOutlet weak var lblInfoText: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var viewGotIt: GradientView!
    
    var memberPlan : MemberPlan!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tapOnBack : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(GenderPopVC.dismissView))
        self.viewBackground.addGestureRecognizer(tapOnBack)
        self.viewGotIt.addGestureRecognizer(tapOnBack)
        setText()
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func setText(){
        
       if let member : MemberPlan = memberPlan {
            switch member {
            case .free:
                lblTitle.text = "Free"
                lblInfoText.text = "Most popular membership\n\nGuest list includes\nUp to 50 selected profile\nin the queue\n\nFull access to see users who\nliked you first"
                
            case .genral:
                lblTitle.text = "General Admission"
                lblInfoText.text = "Most popular membership\n\nGuest list includes\nUp to 50 selected profile\nin the queue\n\nFull access to see users who\nliked you first"
                
            case .guest:
                lblTitle.text = "Guest List"
                lblInfoText.text = "Most popular membership\n\nGuest list includes\nUp to 50 selected profile\nin the queue\n\nFull access to see users who\nliked you first"
                
            case .vip:
                lblTitle.text = "VIP Pass"
                lblInfoText.text = "Most popular membership\n\nGuest list includes\nUp to 50 selected profile\nin the queue\n\nFull access to see users who\nliked you first"
            default:
                lblTitle.text = "Free"
                lblInfoText.text = "Most popular membership\n\nGuest list includes\nUp to 50 selected profile\nin the queue\n\nFull access to see users who\nliked you first"
            }
            
        }else{
            self.lblTitle.text = "Free"
            self.lblInfoText.text = "Most popular membership\n\nGuest list includes\nUp to 50 selected profile\nin the queue\n\nFull access to see users who\nliked you first"
        }
       
    }



}
