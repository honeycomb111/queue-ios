//
//  WelComeVC.swift
//  Queue
//
//  Created by Vivek Padaya on 02/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class WelComeVC: UIViewController {

    @IBOutlet weak var lblTerms: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()

    }
    
    func setupView(){
        let attributedString = NSMutableAttributedString(string: "By signing up for The Queue, you agree to our Terms and Privacy Policy", attributes: [
            .font: UIFont(name: "MyriadPro-Regular", size: 11.0)!,
            .foregroundColor: UIColor.black
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "MyriadPro-Semibold", size: 11.0)!, range: NSRange(location: 46, length: 5))
        attributedString.addAttribute(.font, value: UIFont(name: "MyriadPro-Semibold", size: 11.0)!, range: NSRange(location: 56, length: 14))
        
        lblTerms.attributedText = attributedString
        
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func actnLogin(_ sender: Any) {
        
        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(login, animated: true)
        
    }
    @IBAction func actnSignUp(_ sender: Any) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "PhoneNumberVC") as! PhoneNumberVC
        self.navigationController?.pushViewController(signUp, animated: true)
    }
}
