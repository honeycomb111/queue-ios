
//
//  InnerFeedContentVC.swift
//  Queue
//
//  Created by Vivek Padaya on 18/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class InnerFeedContentVC: UIViewController {
    
    var swipeableView: ZLSwipeableView!
    var acceptBtn : UIButton?
    var rejectBtn : UIButton?
    
    var tempCount = 0
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        swipeableView.nextView = {
            return self.nextCardView()
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        view.backgroundColor = UIColor.white
        view.clipsToBounds = true
        
        
        swipeableView = ZLSwipeableView()
        //        swipeableView.interpretDirection = {(topView: UIView, direction: ZLSwipeableViewDirection, views: [UIView], swipeableView: ZLSwipeableView) in
        //            let programmaticSwipeVelocity = CGFloat(500)
        //            let location = CGPoint(x: topView.center.x-30, y: topView.center.y*0.1)
        //            var directionVector: CGVector?
        //            switch direction {
        //            case ZLSwipeableViewDirection.Left:
        //                directionVector = CGVector(dx: -programmaticSwipeVelocity, dy: 0)
        //            case ZLSwipeableViewDirection.Right:
        //                directionVector = CGVector(dx: programmaticSwipeVelocity, dy: 0)
        //            case ZLSwipeableViewDirection.Up:
        //                directionVector = CGVector(dx: 0, dy: -programmaticSwipeVelocity)
        //            case ZLSwipeableViewDirection.Down:
        //                directionVector = CGVector(dx: 0, dy: programmaticSwipeVelocity)
        //            default:
        //                directionVector = CGVector(dx: 0, dy: 0)
        //            }
        //            return (location, directionVector!)
        //        }
        
        view.addSubview(swipeableView)
        swipeableView.didStart = {view, location in
            print("Did start swiping view at location: \(location)")
        }
        swipeableView.swiping = {view, location, translation in
            print("Swiping at view location: \(location) translation: \(translation)")
        }
        swipeableView.didEnd = {view, location in
            print("Did end swiping view at location: \(location)")
        }
        swipeableView.didSwipe = {view, direction, vector in
            print("Did swipe view in direction: \(direction), vector: \(vector)")
        }
        swipeableView.didCancel = {view in
            print("Did cancel swiping view")
        }
        swipeableView.didTap = {view, location in
            print("Did tap at location \(location)")
        }
        swipeableView.didDisappear = { view in
            print("Did disappear swiping view")
        }
        
        
        swipeableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 80)
        
        //        constrain(swipeableView, view) { view1, view2 in
        //            view1.left == view2.left
        //            view1.right == view2.right
        //            view1.top == view2.top
        //            view1.bottom == view2.bottom
        //        }
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    @objc func actnAccept(){
        print("Accepted")
        swipeableView.swipeTopView(inDirection: .Right)
    }
    
    @objc func actnReject(){
        print("rejected")
        swipeableView.swipeTopView(inDirection: .Left)
    }
    
    // MARK: ()
    func nextCardView() -> UIView? {
        //        if colorIndex >= colors.count {
        //            colorIndex = 0
        //        }
        //
        
        
        let cardView = CardView(frame: swipeableView.bounds)
        
        
        
        //            let contentView = Bundle.main.loadNibNamed("CardContentView", owner: self, options: nil)?.first! as! UIView
        //            contentView.translatesAutoresizingMaskIntoConstraints = false
        //            contentView.backgroundColor = cardView.backgroundColor
        
        let feed = self.storyboard?.instantiateViewController(withIdentifier: "FeedViewVC") as! FeedViewVC
        
        let customView = UIView.init(frame: swipeableView.bounds)
        customView.backgroundColor = .blue
        
        
        customView.addSubview(feed.view)
        customView.clipsToBounds = true
        
        // This is important:
        // https://github.com/zhxnlai/ZLSwipeableView/issues/9
        /*// Alternative:
         let metrics = ["width":cardView.bounds.width, "height": cardView.bounds.height]
         let views = ["contentView": contentView, "cardView": cardView]
         cardView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView(width)]", options: .AlignAllLeft, metrics: metrics, views: views))
         cardView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView(height)]", options: .AlignAllLeft, metrics: metrics, views: views))
         */
        
        
        acceptBtn = UIButton.init(frame: CGRect(x: self.view.frame.width - 36, y: self.view.frame.height - 315, width: 21, height: 21))
        acceptBtn?.setImage(UIImage(named: "ic_accept"), for: .normal)
        acceptBtn?.setTitle("", for: .normal)
        acceptBtn?.addTarget(self, action: #selector(actnAccept), for: .touchUpInside)
        
        
        rejectBtn = UIButton.init(frame: CGRect(x: 15, y: self.view.frame.height - 315, width: 21, height: 21))
        rejectBtn?.setTitle("", for: .normal)
        
        rejectBtn?.setImage(UIImage(named: "ic_reject"), for: .normal)
        rejectBtn?.addTarget(self, action: #selector(actnReject), for: .touchUpInside)
        
        customView.addSubview(acceptBtn!)
        customView.addSubview(rejectBtn!)
        
        
        customView.isUserInteractionEnabled = true
        return customView
    }
    
    

}
