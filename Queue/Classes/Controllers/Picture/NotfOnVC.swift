//
//  NotfOnVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class NotfOnVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nextBtn = UIBarButtonItem.init(title: "Next", style: .done, target: self, action: #selector(nextScreen))
        self.navigationItem.setRightBarButton(nextBtn, animated: true)
        
    }
    
    @objc func nextScreen(){
        
        let member = self.storyboard?.instantiateViewController(withIdentifier: "MembershipListVC") as! MembershipListVC
        self.navigationController?.pushViewController(member, animated: true)
    }

    

   

}
