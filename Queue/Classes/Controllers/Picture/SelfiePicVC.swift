//
//  SelfiePicVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class SelfiePicVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagePicker.delegate = self
        self.setTitle(title: "Profile Settings", subtitle: "")
        
        let nextBtn = UIBarButtonItem.init(title: "Next", style: .done, target: self, action: #selector(nextScreen))
        self.navigationItem.setRightBarButton(nextBtn, animated: true)

    }
    
    @objc func nextScreen(){
        
        let notfic = self.storyboard?.instantiateViewController(withIdentifier: "NotfOnVC") as! NotfOnVC
        self.navigationController?.pushViewController(notfic, animated: true)
    }
    
    @IBAction func actnCamera(_ sender: Any) {
        openCamera()
    }
    
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    //MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            
        }
        picker.dismiss(animated: true, completion: nil)
    }


   

}
