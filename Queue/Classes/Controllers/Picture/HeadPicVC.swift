

//
//  HeadPicVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class HeadPicVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagePicker.delegate = self
        self.navigationController?.navigationBar.isHidden = false
//        self.setTitleText(title: "Profile Setting")
        self.setTitle(title: "Profile Settings", subtitle: "")
        
        let nextBtn = UIBarButtonItem.init(title: "Next", style: .done, target: self, action: #selector(nextScreen))
        self.navigationItem.setRightBarButton(nextBtn, animated: true)
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
    
    @objc func nextScreen(){
    
        let body = self.storyboard?.instantiateViewController(withIdentifier: "BodyPicVC") as! BodyPicVC
        self.navigationController?.pushViewController(body, animated: true)
    }
    
    @IBAction func actnCamera(_ sender: Any) {
        openCamera()
    }
    
    @IBAction func actnUpload(_ sender: Any) {
        openGallary()
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    //MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
           
        }
        picker.dismiss(animated: true, completion: nil)
    }
    

}

