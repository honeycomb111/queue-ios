//
//  FeedBottomInfoVC.swift
//  Queue
//
//  Created by Vivek Padaya on 18/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import SFProgressCircle

class FeedBottomInfoVC: UIViewController , ACTabScrollViewDelegate, ACTabScrollViewDataSource {
    
    @IBOutlet var infoViewOne: UIView!
    @IBOutlet var infoViewTwo: UIView!
    @IBOutlet var infoViewAbout: UIView!
    @IBOutlet var infoViewThree: UIView!
    @IBOutlet var infoViewTags: UIView!
    @IBOutlet var infoViewCompability: UIView!
    
    @IBOutlet weak var tabScrollView: ACTabScrollView!
    var contentViews: [UIView] = []
    
    @IBOutlet weak var progressView: UIView!
    
    var progressBarGradiant : SFCircleGradientView?
    var titleProgress : UILabel?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpTab()
        setUpProgress()
    }
    
    func setUpProgress(){
        
        progressBarGradiant = SFCircleGradientView.init(frame: progressView.bounds)
        progressBarGradiant?.lineWidth = 4
        progressBarGradiant?.progress = 0.7
        
        progressBarGradiant?.startColor = Constants.UIColorFromRGB(r: 255, g: 67, b: 91)
        progressBarGradiant?.endColor = Constants.UIColorFromRGB(r: 38, g: 108, b: 237)
        self.progressView.addSubview(progressBarGradiant!)
        
        titleProgress = UILabel.init(frame: progressView.bounds)
        titleProgress?.text = "70%"
        titleProgress?.textAlignment = .center
        titleProgress?.font = Constants.defaultBoldFont(size: 11)
        self.progressView.addSubview(titleProgress!)
        
        
        

    }
    
    
    
    
    func setUpTab(){
        tabScrollView.defaultPage = 0
        tabScrollView.arrowIndicator = false
        tabScrollView.tabSectionHeight = 5
        tabScrollView.tabSectionBackgroundColor = UIColor.clear
        tabScrollView.contentSectionBackgroundColor = UIColor.clear
        //        tabScrollView.tabGradient = true
        //        tabScrollView.pagingEnabled = true
        //        tabScrollView.cachedPageLimit = 3
        
        tabScrollView.delegate = self
        tabScrollView.dataSource = self
        
        // create content views from storyboard
        //        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        for i in 0..<6 {
            if i == 0 {
                contentViews.append(self.infoViewOne)
            }else if i == 1{
                contentViews.append(self.infoViewTwo)
            }else if i == 2{
                contentViews.append(self.infoViewThree)
            }else if i == 3{
                contentViews.append(self.infoViewAbout)
            }else if i == 4{
                contentViews.append(self.infoViewTags)
            }else if i == 5{
                contentViews.append(self.infoViewCompability)
            }
            
            
        }
    }
    
   
    
    @IBAction func actnCompabiltyInfo(_ sender: Any) {
        let compPop = self.storyboard?.instantiateViewController(withIdentifier: "CompabiltyInfoVC") as! CompabiltyInfoVC
        compPop.modalPresentationStyle = .overCurrentContext
        compPop.modalTransitionStyle = .crossDissolve
        
        self.present(compPop, animated: true, completion: nil)
    }
    
    
    // MARK: ACTabScrollViewDelegate
    func tabScrollView(_ tabScrollView: ACTabScrollView, didChangePageTo index: Int) {
        print(index)
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, didScrollPageTo index: Int) {
    }
    
    // MARK: ACTabScrollViewDataSource
    func numberOfPagesInTabScrollView(_ tabScrollView: ACTabScrollView) -> Int {
        return 6
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, tabViewForPageAtIndex index: Int) -> UIView {
        // create a label
        
        let viewIcon = UIView.init(frame: CGRect(x: 0, y: 0, width: 64, height: 10))
        viewIcon.backgroundColor = .blue
        return viewIcon
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, contentViewForPageAtIndex index: Int) -> UIView {
        return contentViews[index]
    }
    
    
    func getRect() -> CGRect {
        return CGRect(
            x: view.frame.origin.x + 15,
            y: (view.frame.size.height - view.frame.size.width) / 2,
            width: view.frame.size.width - 30,
            height: view.frame.size.width - 30)
    }

}
