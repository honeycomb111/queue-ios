//
//  LifeStyleContentVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

enum LifeStyleType : Int {
    case kid = 0
    case pet = 1
    case health = 2
    case food = 3
    case drinking = 4
    case smoking = 5
}

class LifeStyleContentVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var viewTextField: UIView!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var viewNext: UIView!
    
    var lifestyleTab : LifestyleTabBar?
    
    var userModel : UserModel = UserModel.init()
    
    var lifeStyleType : LifeStyleType?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setGradiant()
        setUpView()
        txtField.delegate = self
        
    }
    func setGradiant(){
        self.viewTextField.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        
    }
    
    func setUpView(){
        
        
        switch lifeStyleType! {
        case .kid:
            txtField.attributedPlaceholder = NSAttributedString(string: "Kids",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        case .pet:
            txtField.attributedPlaceholder = NSAttributedString(string: "Pets",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        case .health:
            txtField.attributedPlaceholder = NSAttributedString(string: "Health",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        case .food:
            txtField.attributedPlaceholder = NSAttributedString(string: "Food",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        case .drinking:
            txtField.attributedPlaceholder = NSAttributedString(string: "Drinking",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        case .smoking:
            txtField.attributedPlaceholder = NSAttributedString(string: "Smoking",
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        default:
            self.txtField.placeholder = ""
        }
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        switch lifeStyleType! {
        case .kid:
            let quaPop = self.storyboard?.instantiateViewController(withIdentifier: "LifestylePopVC") as! LifestylePopVC
            quaPop.delegate = self
            quaPop.lifeStyleType = lifeStyleType
            quaPop.modalPresentationStyle = .overCurrentContext
            quaPop.modalTransitionStyle = .crossDissolve
            
            self.present(quaPop, animated: true, completion: nil)
        case .pet:
            let quaPop = self.storyboard?.instantiateViewController(withIdentifier: "LifestylePopVC") as! LifestylePopVC
            quaPop.delegate = self
            quaPop.lifeStyleType = lifeStyleType
            quaPop.modalPresentationStyle = .overCurrentContext
            quaPop.modalTransitionStyle = .crossDissolve
            
            self.present(quaPop, animated: true, completion: nil)
        case .health:
            return true
        case .food:
            return true

        case .drinking:
            let quaPop = self.storyboard?.instantiateViewController(withIdentifier: "LifestylePopVC") as! LifestylePopVC
            quaPop.delegate = self
            quaPop.lifeStyleType = lifeStyleType
            quaPop.modalPresentationStyle = .overCurrentContext
            quaPop.modalTransitionStyle = .crossDissolve
            
            self.present(quaPop, animated: true, completion: nil)
        case .smoking:
            let quaPop = self.storyboard?.instantiateViewController(withIdentifier: "LifestylePopVC") as! LifestylePopVC
            quaPop.delegate = self
            quaPop.lifeStyleType = lifeStyleType
            quaPop.modalPresentationStyle = .overCurrentContext
            quaPop.modalTransitionStyle = .crossDissolve
            
            self.present(quaPop, animated: true, completion: nil)

        default:
            return true
        }
        
        return false
    }
    
   
    
    

    @IBAction func actnNext(_ sender: Any) {
       
        
        let index = lifeStyleType?.rawValue ?? 0
        
        
        if lifeStyleType == .smoking {

            let poltical = self.storyboard?.instantiateViewController(withIdentifier: "PoliticalTabBar") as! PoliticalTabBar
            self.navigationController?.pushViewController(poltical, animated: true)
        }else{
            self.lifestyleTab?.userModel = userModel
            self.lifestyleTab?.tabScrollView.changePageToIndex(index + 1, animated: true)
        }
        
    }
    

}
