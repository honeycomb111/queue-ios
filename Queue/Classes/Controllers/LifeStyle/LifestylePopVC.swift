//
//  LifestylePopVC.swift
//  Queue
//
//  Created by Vivek Padaya on 12/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class LifestylePopVC: UIViewController {

    var lifeStyleType : LifeStyleType?

    var delegate : LifeStyleContentVC?
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapOnBack : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(LifestylePopVC.dismissView))
        self.viewBackground.addGestureRecognizer(tapOnBack)
        setUpView()
    }
    
    func setUpView(){
        
        switch lifeStyleType! {
        case .kid:
            self.lblTitle.text = "Kids"
        case .pet:
            self.lblTitle.text = "Pets"
        case .health:
            self.lblTitle.text = "Health"
        case .food:
            self.lblTitle.text = "Food"
        case .drinking:
            self.lblTitle.text = "Drinking"
        case .smoking:
            self.lblTitle.text = "Smoking"
        default:
            self.lblTitle.text = ""
        }
    }
    
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    

    

}
