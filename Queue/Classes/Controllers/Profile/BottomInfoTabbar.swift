//
//  BottomInfoTabbar.swift
//  Queue
//
//  Created by Vivek Padaya on 14/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import SFProgressCircle

class BottomInfoTabbar: UIViewController, ACTabScrollViewDelegate, ACTabScrollViewDataSource, UITextFieldDelegate {

    @IBOutlet var infoViewOne: UIView!
    @IBOutlet var infoViewTwo: UIView!
    @IBOutlet var infoViewAbout: UIView!
    @IBOutlet var infoViewThree: UIView!
    @IBOutlet var infoViewTags: UIView!
    
    @IBOutlet weak var tabScrollView: ACTabScrollView!
    var contentViews: [UIView] = []
    
    @IBOutlet weak var progressView: UIView!
    
    var progressBarGradiant : SFCircleGradientView?
    var titleProgress : UILabel?
    
    @IBOutlet var viewTableView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var selectedIndexPath : IndexPath = IndexPath()
    
    var queArray : [String] = [String]()
    var defaultAnsArray : [String] = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpTab()
        setUpProgress()
        
        self.tableView.register(UINib(nibName: "QueAnsViewCell", bundle: nil), forCellReuseIdentifier: "QueAnsViewCell")
        tableView.reorder.delegate = self
        
        queArray = ["What is your favourite food?","What is your favourite place?","What is your favourite place?","What is your favourite fruit?","What is your favourite colour?","Favourite dinner guest?","I am looking for?","The last time i cried?","Where to find me at party?","Unusal skills?","My childhood crush?"]
         defaultAnsArray = ["Chiness","Amsterdam","France","Mango","Cherry red","Friends","A serious relationship","Last week","Near DJ","Good sense of humor","4th grade theacher"]
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none

        
    }
    
    func setUpProgress(){
        
        progressBarGradiant = SFCircleGradientView.init(frame: progressView.bounds)
        progressBarGradiant?.lineWidth = 4
        progressBarGradiant?.progress = 0.7
        
        progressBarGradiant?.startColor = Constants.UIColorFromRGB(r: 255, g: 67, b: 91)
        progressBarGradiant?.endColor = Constants.UIColorFromRGB(r: 38, g: 108, b: 237)
        self.progressView.addSubview(progressBarGradiant!)
        
        titleProgress = UILabel.init(frame: progressView.bounds)
        titleProgress?.text = "70%"
        titleProgress?.textAlignment = .center
        titleProgress?.font = Constants.defaultBoldFont(size: 11)
        self.progressView.addSubview(titleProgress!)
        
        
    }

    func setUpTab(){
        tabScrollView.defaultPage = 0
        tabScrollView.arrowIndicator = false
        tabScrollView.tabSectionHeight = 5
        tabScrollView.tabSectionBackgroundColor = UIColor.clear
        tabScrollView.contentSectionBackgroundColor = UIColor.clear
        //        tabScrollView.tabGradient = true
        //        tabScrollView.pagingEnabled = true
        //        tabScrollView.cachedPageLimit = 3
        
        tabScrollView.delegate = self
        tabScrollView.dataSource = self
        
        // create content views from storyboard
        //        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        for i in 0..<5 {
            if i == 0 {
                contentViews.append(self.infoViewOne)
            }else if i == 1{
                contentViews.append(self.infoViewTwo)
            }else if i == 2{
                contentViews.append(self.infoViewThree)
            }else if i == 3{
                contentViews.append(self.infoViewAbout)
            }else if i == 4{
                
                let bothViews = UIView.init(frame: UIScreen.main.bounds)
                self.viewTableView.frame = UIScreen.main.bounds
                bothViews.addSubview(self.viewTableView)
                self.viewTableView.isHidden = true
                bothViews.addSubview(self.infoViewTags)
                self.contentViews.append(bothViews)
            }
            
            
        }
    }
    
    // MARK: ACTabScrollViewDelegate
    func tabScrollView(_ tabScrollView: ACTabScrollView, didChangePageTo index: Int) {
        print(index)
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, didScrollPageTo index: Int) {
    }
    
    // MARK: ACTabScrollViewDataSource
    func numberOfPagesInTabScrollView(_ tabScrollView: ACTabScrollView) -> Int {
        return 5
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, tabViewForPageAtIndex index: Int) -> UIView {
        // create a label
        
        let viewIcon = UIView.init(frame: CGRect(x: 0, y: 0, width: 64, height: 10))
        viewIcon.backgroundColor = .blue
        return viewIcon
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, contentViewForPageAtIndex index: Int) -> UIView {
        return contentViews[index]
    }
    
    
  
    

}

extension BottomInfoTabbar : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath {
            return 100
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedIndexPath == indexPath {
            print("Nothing")
        }else{
            selectedIndexPath = indexPath
            tableView.reloadRows(at: [indexPath], with: .top)
            self.view.endEditing(true)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.count > 0 {
            if let text = textField.text, text.count > 0 {
                textField.backgroundColor = .white
            }
        }else{
            if let text = textField.text, text.count == 0 {
                textField.backgroundColor = .clear
            }
        }
        return true
    }
    
    
}

extension BottomInfoTabbar : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let spacer = tableView.reorder.spacerCell(for: indexPath) {
            return spacer
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "QueAnsViewCell", for: indexPath) as! QueAnsViewCell
        
        cell.selectionStyle = .none
    
        cell.lblQue.text = queArray[indexPath.row]
        cell.txtAnswer.text = defaultAnsArray[indexPath.row]
        cell.txtAnswer.tag = indexPath.row
        cell.txtAnswer.delegate = self
        cell.clipsToBounds = true
        
        return cell
    }
}
extension BottomInfoTabbar: TableViewReorderDelegate {
    
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = queArray[sourceIndexPath.row]
        queArray.remove(at: sourceIndexPath.row)
        queArray.insert(item, at: destinationIndexPath.row)
        
        let itemAns = defaultAnsArray[sourceIndexPath.row]
        defaultAnsArray.remove(at: sourceIndexPath.row)
        defaultAnsArray.insert(itemAns, at: destinationIndexPath.row)
    }
    
}
