//
//  QueAnsViewCell.swift
//  Queue
//
//  Created by Vivek Padaya on 14/01/19.
//  Copyright © 2019 Vivek Padaya. All rights reserved.
//

import UIKit

class QueAnsViewCell: UITableViewCell {

    @IBOutlet weak var lblQue: UILabel!
    @IBOutlet weak var txtAnswer: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
