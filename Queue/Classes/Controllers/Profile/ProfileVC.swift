//
//  ProfileVC.swift
//  Queue
//
//  Created by Vivek Padaya on 14/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import ImageSlideshow
import CTSlidingUpPanel

class ProfileVC: UIViewController, CTBottomSlideDelegate {

    @IBOutlet weak var slideshow: ImageSlideshow!
    var containerVC: BottomInfoTabbar?

     let localSource = [ImageSource(imageString: "media")!, ImageSource(imageString: "media")!, ImageSource(imageString: "media")!, ImageSource(imageString: "media")!]
    
    @IBOutlet weak var bottomView: UIView!
    var bottomController:CTBottomSlideController?;

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupSlider()
        setupBottom()
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    func setupBottom(){
        bottomController = CTBottomSlideController(parent: view, bottomView: bottomView, tabController: self.tabBarController!, navController: self.navigationController, visibleHeight: 200)
        
        
        bottomController?.setAnchorPoint(anchor: 0.7)
        bottomController?.delegate = self;
        
        bottomController?.onPanelExpanded = {
            print("Panel Expanded in closure")
//            if self.containerVC?.tabScrollView.pageIndex == 4 {
                self.containerVC?.infoViewTags.isHidden = true
                self.containerVC?.viewTableView.isHidden = false
//            }
        }
        
        bottomController?.onPanelCollapsed = {
            print("Panel Collapsed in closure")
//            if self.containerVC?.tabScrollView.pageIndex == 4 {
                self.containerVC?.infoViewTags.isHidden = false
                self.containerVC?.viewTableView.isHidden = true
//            }
        }
        
        bottomController?.onPanelMoved = { offset in
            print("Panel moved in closure " + offset.description)
        }
        
        //Uncomment to specify top margin on expanded panel
        //bottomController?.setExpandedTopMargin(pixels: 100)
        
        if bottomController?.currentState == .collapsed
        {
            //do anything, i don't care
        }
        
    }

    func setupSlider(){
        
        slideshow.slideshowInterval = 5.0
//        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 210))
        slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.black
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        slideshow.pageIndicator = pageControl
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            print("current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        slideshow.setImageInputs(localSource)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.didTap))
        slideshow.addGestureRecognizer(recognizer)
    }
    
    @objc func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    
    //MARK :- Bottom delegate
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        bottomController?.viewWillTransition(to: size, with: coordinator)
    }
    

    func didPanelCollapse()
    {
        print("Collapsed");
    }
    func didPanelExpand(){
        print("Expanded")
    }
    func didPanelAnchor(){
        print("Anchored")
    }
    
    func didPanelMove(panelOffset: CGFloat)
    {
        print(panelOffset);
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "bottomSegue" {
            if let vc = segue.destination as? BottomInfoTabbar {
                self.containerVC = vc
            }
        }
    }


}
