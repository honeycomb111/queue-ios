//
//  PasswordVC.swift
//  Queue
//
//  Created by Vivek Padaya on 03/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class PasswordVC: UIViewController {

    @IBOutlet weak var txtPwd: UITextField!
    @IBOutlet weak var txtCnfrmPwd: UITextField!
    
    @IBOutlet weak var viewPwd: UIView!
    @IBOutlet weak var viewCnfrmPwd: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        setGradiantTextField()
        
    }
    
    func setGradiantTextField(){
        
        // Phone number
        self.viewCnfrmPwd.layer.cornerRadius = 10.0
        self.viewCnfrmPwd.clipsToBounds = true
        
        
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.viewCnfrmPwd.frame.size)
        gradient.colors = [Constants.UIColorFromRGB(r: 255, g: 105, b: 105).cgColor, Constants.UIColorFromRGB(r: 249, g: 155, b: 155).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.path = UIBezierPath(roundedRect: self.viewCnfrmPwd.bounds, cornerRadius: 10.0).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        
        gradient.mask = shape
        
        self.viewCnfrmPwd.layer.addSublayer(gradient)
        
        
        // Password
        self.viewPwd.layer.cornerRadius = 10.0
        self.viewPwd.clipsToBounds = true
        
        
        let gradientNew = CAGradientLayer()
        gradientNew.frame =  CGRect(origin: CGPoint.zero, size: self.viewPwd.frame.size)
        gradientNew.colors = [Constants.UIColorFromRGB(r: 255, g: 105, b: 105).cgColor, Constants.UIColorFromRGB(r: 249, g: 155, b: 155).cgColor]
        gradientNew.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientNew.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let shapeNew = CAShapeLayer()
        shapeNew.lineWidth = 2
        shapeNew.path = UIBezierPath(roundedRect: self.viewPwd.bounds, cornerRadius: 10.0).cgPath
        shapeNew.strokeColor = UIColor.black.cgColor
        shapeNew.fillColor = UIColor.clear.cgColor
        
        gradientNew.mask = shapeNew
        
        self.viewPwd.layer.addSublayer(gradientNew)
        
    }
    

    
    @IBAction func actnNext(_ sender: Any) {
        let getStart = self.storyboard?.instantiateViewController(withIdentifier: "GetStartVC") as! GetStartVC
        self.navigationController?.pushViewController(getStart, animated: true)
    }
    
}
