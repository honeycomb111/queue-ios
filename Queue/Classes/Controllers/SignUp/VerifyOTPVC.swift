//
//  VerifyOTPVC.swift
//  Queue
//
//  Created by Vivek Padaya on 03/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class VerifyOTPVC: UIViewController {

    @IBOutlet weak var viewCode: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        self.setGradiant()
    }
    
    func setGradiant(){
        self.viewCode.layer.cornerRadius = 10.0
        self.viewCode.clipsToBounds = true
        
        
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.viewCode.frame.size)
        gradient.colors = [Constants.UIColorFromRGB(r: 255, g: 105, b: 105).cgColor, Constants.UIColorFromRGB(r: 249, g: 155, b: 155).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.path = UIBezierPath(roundedRect: self.viewCode.bounds, cornerRadius: 10.0).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        
        gradient.mask = shape
        
        self.viewCode.layer.addSublayer(gradient)
    }
    
    
    @IBAction func actnNext(_ sender: Any) {
        let pwd = self.storyboard?.instantiateViewController(withIdentifier: "PasswordVC") as! PasswordVC
        self.navigationController?.pushViewController(pwd, animated: true)
    }
    
   

}
