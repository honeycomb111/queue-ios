//
//  GetStartVC.swift
//  Queue
//
//  Created by Vivek Padaya on 03/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class GetStartVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actnGetStart(_ sender: Any) {
        let personalTab = self.storyboard?.instantiateViewController(withIdentifier: "PersonalDataTabVC") as! PersonalDataTabVC
        self.navigationController?.pushViewController(personalTab, animated: true)
    }
    
}
