//
//  PhoneNumberVC.swift
//  Queue
//
//  Created by Vivek Padaya on 02/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import SKCountryPicker


class PhoneNumberVC: UIViewController {
    @IBOutlet var viewCountry: UIView!
    @IBOutlet weak var txtMobNo: UITextField!
    @IBOutlet weak var viewPhone: UIView!
    
    let contryPickerController = CountryPickerController()
    
    @IBOutlet var countryImg: UIImageView!
    @IBOutlet var countryCode: UILabel!
    
    var countryCodeStr : String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        setGradiant()
        
        let country = CountryManager.shared.currentCountry
        
        
        countryCode.text = country?.countryCode
        countryImg.image = country?.flag
        self.countryCodeStr = country?.dialingCode
    }
    

    func setGradiant(){
        self.viewPhone.layer.cornerRadius = 10.0
        self.viewPhone.clipsToBounds = true
        
        
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.viewPhone.frame.size)
        gradient.colors = [Constants.UIColorFromRGB(r: 255, g: 105, b: 105).cgColor, Constants.UIColorFromRGB(r: 249, g: 155, b: 155).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.path = UIBezierPath(roundedRect: self.viewPhone.bounds, cornerRadius: 10.0).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
       
        gradient.mask = shape
        
        self.viewPhone.layer.addSublayer(gradient)
    }
   
    @IBAction func actnCountry(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
            self.countryImg.image = country.flag
            self.countryCode.text = country.countryCode
            self.countryCodeStr = country.dialingCode
            
        }
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
    }
    
    @IBAction func actnNext(_ sender: Any) {
        let otp = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
        self.navigationController?.pushViewController(otp, animated: true)
    }
}
