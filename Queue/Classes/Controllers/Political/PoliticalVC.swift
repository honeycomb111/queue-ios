//
//  PoliticalVC.swift
//  Queue
//
//  Created by Vivek Padaya on 13/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class PoliticalVC: UIViewController {

    var politicalTab : PoliticalTabBar?
    
    var userModel : UserModel = UserModel.init()
    
    @IBOutlet weak var txtPolitical: UITextField!
    @IBOutlet weak var viewPoltical: UIView!
    @IBOutlet weak var viewNext: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setGradiant()
    }
    func setGradiant(){
        self.viewPoltical.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
    }
    


    @IBAction func actnNext(_ sender: Any) {
        self.politicalTab?.userModel = userModel
        self.politicalTab?.tabScrollView.changePageToIndex(1, animated: true)
    }
    

}
