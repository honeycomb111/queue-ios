//
//  ReligionVC.swift
//  Queue
//
//  Created by Vivek Padaya on 13/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class ReligionVC: UIViewController {
    var politicalTab : PoliticalTabBar?
    
    var userModel : UserModel = UserModel.init()

    
    @IBOutlet weak var viewReligion: UIView!
    @IBOutlet weak var txtReligion: UITextField!
    
    @IBOutlet weak var viewNext: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setGradiant()
    }
    
    @IBAction func actnNext(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.openHome()
    }
    
    
    func setGradiant(){
        self.viewReligion.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        
    }
    


   

}
