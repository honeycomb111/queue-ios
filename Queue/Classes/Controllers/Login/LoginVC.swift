//
//  LoginVC.swift
//  Queue
//
//  Created by Vivek Padaya on 01/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

 
   
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewPwd: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        setGradiantTextField()

    }
    
    func setGradiantTextField(){
        
        // Phone number
        self.viewPhone.layer.cornerRadius = 10.0
        self.viewPhone.clipsToBounds = true
        
        
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.viewPhone.frame.size)
        gradient.colors = [Constants.UIColorFromRGB(r: 255, g: 105, b: 105).cgColor, Constants.UIColorFromRGB(r: 249, g: 155, b: 155).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.path = UIBezierPath(roundedRect: self.viewPhone.bounds, cornerRadius: 10.0).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        
        gradient.mask = shape
        
        self.viewPhone.layer.addSublayer(gradient)
        
        
        // Password
        self.viewPwd.layer.cornerRadius = 10.0
        self.viewPwd.clipsToBounds = true
        
        
        let gradientNew = CAGradientLayer()
        gradientNew.frame =  CGRect(origin: CGPoint.zero, size: self.viewPwd.frame.size)
        gradientNew.colors = [Constants.UIColorFromRGB(r: 255, g: 105, b: 105).cgColor, Constants.UIColorFromRGB(r: 249, g: 155, b: 155).cgColor]
        gradientNew.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientNew.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let shapeNew = CAShapeLayer()
        shapeNew.lineWidth = 2
        shapeNew.path = UIBezierPath(roundedRect: self.viewPwd.bounds, cornerRadius: 10.0).cgPath
        shapeNew.strokeColor = UIColor.black.cgColor
        shapeNew.fillColor = UIColor.clear.cgColor
        
        gradientNew.mask = shapeNew
        
        self.viewPwd.layer.addSublayer(gradientNew)

    }
    
    
    

    @IBAction func actnSignUp(_ sender: Any) {
        
        let phone = self.storyboard?.instantiateViewController(withIdentifier: "PhoneNumberVC") as! PhoneNumberVC
        self.navigationController?.pushViewController(phone, animated: true)
    }
    
}
