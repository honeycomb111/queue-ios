
//
//  FilterObjectiveScreenVC.swift
//  Queue
//
//  Created by Vivek Padaya on 21/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class FilterObjectiveScreenVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtObjective: UITextField!
    
    var objectiveTabvc : FilterTypeTabVC?
    
    
    @IBOutlet weak var viewNext: UIView!
    @IBOutlet weak var viewObjective: UIView!
    
    
    var userModel : UserModel = UserModel.init()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.viewObjective.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 255, g: 105, b: 105), endColor: Constants.UIColorFromRGB(r: 249, g: 155, b: 155))
        self.viewNext.setBoarderGradiantWithColor(startColor: Constants.UIColorFromRGB(r: 0, g: 122, b: 255), endColor: Constants.UIColorFromRGB(r: 145, g: 195, b: 233))
        // Do any additional setup after loading the view.
        self.txtObjective.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let typePop = self.storyboard?.instantiateViewController(withIdentifier: "FilterObjectiveVC") as! FilterObjectiveVC
        typePop.modalPresentationStyle = .overCurrentContext
        typePop.modalTransitionStyle = .crossDissolve
        
        self.present(typePop, animated: true, completion: nil)

        return false
    }

    
    @IBAction func actnNext(_ sender: Any) {
        
        objectiveTabvc?.navigationController?.popViewController(animated: true)
    }
    
   
}
