//
//  FilterTwoPageVC.swift
//  Queue
//
//  Created by Vivek Padaya on 18/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class FilterTwoPageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actnNationality(_ sender: Any) {
        let nationalityPop = self.storyboard?.instantiateViewController(withIdentifier: "NationalityAllowVC") as! NationalityAllowVC
        nationalityPop.modalPresentationStyle = .overCurrentContext
        nationalityPop.modalTransitionStyle = .crossDissolve
        
        self.present(nationalityPop, animated: true, completion: nil)

    }
    
    @IBAction func actnEthnicity(_ sender: Any) {
        let ethnicityPop = self.storyboard?.instantiateViewController(withIdentifier: "EthnicityVC") as! EthnicityVC
        ethnicityPop.modalPresentationStyle = .overCurrentContext
        ethnicityPop.modalTransitionStyle = .crossDissolve
        
        self.present(ethnicityPop, animated: true, completion: nil)

    }
    

}
