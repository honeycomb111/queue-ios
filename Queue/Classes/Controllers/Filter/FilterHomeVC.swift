//
//  FilterHomeVC.swift
//  Queue
//
//  Created by Vivek Padaya on 18/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class FilterHomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

 
    @IBAction func actnType(_ sender: Any) {
//        let typePop = self.storyboard?.instantiateViewController(withIdentifier: "FilterTypeSelectionVC") as! FilterTypeSelectionVC
//        typePop.modalPresentationStyle = .overCurrentContext
//        typePop.modalTransitionStyle = .crossDissolve
//
//        self.present(typePop, animated: true, completion: nil)
        
        let tabType = self.storyboard?.instantiateViewController(withIdentifier: "FilterTypeTabVC") as! FilterTypeTabVC
        self.navigationController?.pushViewController(tabType, animated: true)

    }
    
    @IBAction func actnFilter(_ sender: Any) {
        
        let main = self.storyboard?.instantiateViewController(withIdentifier: "FilterObjectiveMainVC") as! FilterObjectiveMainVC
        self.navigationController?.pushViewController(main, animated: true)
    }
    
    @IBAction func actnObjective(_ sender: Any) {
        let typePop = self.storyboard?.instantiateViewController(withIdentifier: "FilterObjectiveVC") as! FilterObjectiveVC
        typePop.modalPresentationStyle = .overCurrentContext
        typePop.modalTransitionStyle = .crossDissolve
        
        self.present(typePop, animated: true, completion: nil)
    }
    
}
