//
//  FilterTypeTabVC.swift
//  Queue
//
//  Created by Vivek Padaya on 21/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class FilterTypeTabVC: UIViewController , ACTabScrollViewDelegate, ACTabScrollViewDataSource {
    
    @IBOutlet weak var tabScrollView: ACTabScrollView!
    var contentViews: [UIView] = []
    
    var iconNameArray : [String] = ["iconThree", "ic_religion"]
    
    var userModel : UserModel = UserModel.init()


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpTab()
        self.setTitle(title: "Swipe your Type", subtitle: "")
    }
    

    func setUpTab(){
        tabScrollView.defaultPage = 0
        tabScrollView.arrowIndicator = false
        tabScrollView.tabSectionHeight = 64
        //        tabScrollView.tabSectionBackgroundColor = UIColor.whiteColor()
        //        tabScrollView.contentSectionBackgroundColor = UIColor.whiteColor()
        //        tabScrollView.tabGradient = true
        //        tabScrollView.pagingEnabled = true
        //        tabScrollView.cachedPageLimit = 3
        
        tabScrollView.delegate = self
        tabScrollView.dataSource = self
        
        // create content views from storyboard
        //        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        for i in 0..<2 {
            if i == 0 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "GenderVC") as! GenderVC
                vc.objectiveTabvc = self
                vc.isFromFilter = true
                addChild(vc) // don't forget, it's very important
                contentViews.append(vc.view)
            }
//            else if i == 1{
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterObjectiveScreenVC") as! FilterObjectiveScreenVC
//                vc.objectiveTabvc = self
//                addChild(vc) // don't forget, it's very important
//                contentViews.append(vc.view)
//            }
            
            
        }
    }
    
    // MARK: ACTabScrollViewDelegate
    func tabScrollView(_ tabScrollView: ACTabScrollView, didChangePageTo index: Int) {
        print(index)
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, didScrollPageTo index: Int) {
    }
    
    // MARK: ACTabScrollViewDataSource
    func numberOfPagesInTabScrollView(_ tabScrollView: ACTabScrollView) -> Int {
        return 1
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, tabViewForPageAtIndex index: Int) -> UIView {
        // create a label
        
        let viewIcon = UIView.init(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        let iconView = UIImageView.init(frame: CGRect(x: 7, y: 7, width: 50, height: 50))
        
        iconView.image = UIImage(named: iconNameArray[index])
        viewIcon.addSubview(iconView)
        return viewIcon
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, contentViewForPageAtIndex index: Int) -> UIView {
        return contentViews[index]
    }
    
    
    


}
