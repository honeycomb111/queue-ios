//
//  FilterTypeSelectionVC.swift
//  Queue
//
//  Created by Vivek Padaya on 18/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class FilterTypeSelectionVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnOptnOne: UIButton!
    @IBOutlet weak var btnOptnTwo: UIButton!
    
    var isGenderSelected = false
    
    @IBOutlet weak var viewBackground: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpType()
        let tapOnBack : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(GenderPopVC.dismissView))
        self.viewBackground.addGestureRecognizer(tapOnBack)
        
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func setUpType(){
        self.lblTitle.text = "Looking For"
        self.btnOptnOne.setTitle("Gender", for: .normal)
        self.btnOptnTwo.setTitle("Objective", for: .normal)
    }
    
    func setUpGender(){
        self.lblTitle.text = "Gender"
        self.btnOptnOne.setTitle("Male", for: .normal)
        self.btnOptnTwo.setTitle("Female", for: .normal)
    }
    
    @IBAction func actnOptnOne(_ sender: Any) {
        if !isGenderSelected {
            isGenderSelected = true
            setUpGender()
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func actnOptnTwo(_ sender: Any) {
        
        if !isGenderSelected {
            self.dismiss(animated: true) {
                let typePop = self.storyboard?.instantiateViewController(withIdentifier: "FilterObjectiveVC") as! FilterObjectiveVC
                typePop.modalPresentationStyle = .overCurrentContext
                typePop.modalTransitionStyle = .crossDissolve
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window!.rootViewController?.present(typePop, animated: true, completion: nil)
//                self.present(typePop, animated: true, completion: nil)
            }
           
        }else{
            dismissView()

        }
        
    }
    

}
