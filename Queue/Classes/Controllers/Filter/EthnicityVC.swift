//
//  EthnicityVC.swift
//  Queue
//
//  Created by Vivek Padaya on 19/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class EthnicityVC: UIViewController {

    @IBOutlet weak var viewBackground: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapOnBack : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissView))
        self.viewBackground.addGestureRecognizer(tapOnBack)
        
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actnNo(_ sender: Any) {
        dismissView()
    }
    
    @IBAction func actnYes(_ sender: Any) {
        dismissView()
    }
    

}
