//
//  FilterObjectiveMainVC.swift
//  Queue
//
//  Created by Vivek Padaya on 18/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class FilterObjectiveMainVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setTitle(title: "Filters", subtitle: "")

        let rightBarBtn = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(actnDone))
        self.navigationItem.rightBarButtonItem = rightBarBtn
        
    }
    
    @objc func actnDone(){
        print("Done button pressed")
        
    }
  

}
