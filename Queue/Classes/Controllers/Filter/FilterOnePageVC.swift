//
//  FilterOnePageVC.swift
//  Queue
//
//  Created by Vivek Padaya on 18/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit
import MultiSlider

class FilterOnePageVC: UIViewController {

    @IBOutlet weak var lblKmInfo: UILabel!
    @IBOutlet weak var sliderKm: UISlider!
    
    @IBOutlet weak var lblAgeInfo: UILabel!
    @IBOutlet weak var sliderAge: MultiSlider!
    
    
    @IBOutlet weak var lblHeightInfo: UILabel!
    @IBOutlet weak var sliderHeight: MultiSlider!
    
    @IBOutlet weak var viewEducation: UIView!
    
    @IBOutlet weak var viewObjective: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpMultiSlider()
        
        viewEducation.layer.borderWidth = 1
        viewEducation.layer.borderColor = UIColor.lightGray.cgColor
        
        viewObjective.layer.borderWidth = 1
        viewObjective.layer.borderColor = UIColor.lightGray.cgColor
        
        let tapOnCollege = UITapGestureRecognizer.init(target: self, action: #selector(actnCollege))
        self.viewEducation.addGestureRecognizer(tapOnCollege)
        
        let tapOnObj = UITapGestureRecognizer.init(target: self, action: #selector(actnObjective))
        self.viewObjective.addGestureRecognizer(tapOnObj)
        
        
    }
    
    @objc func actnObjective(){
        let typePop = self.storyboard?.instantiateViewController(withIdentifier: "FilterObjectiveVC") as! FilterObjectiveVC
        typePop.modalPresentationStyle = .overCurrentContext
        typePop.modalTransitionStyle = .crossDissolve
        
        self.present(typePop, animated: true, completion: nil)
    }
    
    @objc func actnCollege(){
        let quaPop = self.storyboard?.instantiateViewController(withIdentifier: "QualificationPopVC") as! QualificationPopVC
        quaPop.modalPresentationStyle = .overCurrentContext
        quaPop.modalTransitionStyle = .crossDissolve
        
        self.present(quaPop, animated: true, completion: nil)
        
    }
    
    func setUpMultiSlider(){
        
        let horizontalMultiSlider = MultiSlider()
        horizontalMultiSlider.orientation = .horizontal
        horizontalMultiSlider.minimumValue = 18
        horizontalMultiSlider.maximumValue = 45
        horizontalMultiSlider.thumbCount = 2
//        horizontalMultiSlider.hi
//        horizontalMultiSlider.valueLabelPosition = .bottom
        
        horizontalMultiSlider.frame = CGRect(x: 0, y: 0, width: sliderAge.frame.width, height: sliderAge.frame.height)
        horizontalMultiSlider.addTarget(self, action: #selector(sliderAgeChanged(slider:)), for: .valueChanged)
        sliderAge.addSubview(horizontalMultiSlider)
        
//        sliderAge.minimumValue = 18    // default is 0.0
//        sliderAge.maximumValue = 45    // default is 1.0
//        sliderAge.snapStepSize = 18  // default is 0.0, i.e. don't snap
        
        
//        sliderAge.addTarget(self, action: #selector(sliderAgeChanged(slider:)), for: .valueChanged) // continuous changes
//        sliderAge.disabledThumbIndices = [2]
//        sliderAge.value = [20, 25]
//        sliderAge.valueLabelPosition = .top


//        sliderHeight.minimumValue = 100    // default is 0.0
//        sliderHeight.maximumValue = 200    // default is 1.0
//        sliderHeight.snapStepSize = 100  // default is 0.0, i.e. don't snap
//
//        sliderHeight.value = [145, 165]
//
//        sliderHeight.addTarget(self, action: #selector(sliderHeightChanged(slider:)), for: .valueChanged) // continuous changes
    }
    
    
    
    @objc func sliderAgeChanged(slider: MultiSlider) {
        print("\(slider.value)") // e.g., [1.0, 4.5, 5.0]
    }
    
    @objc func sliderHeightChanged(slider: MultiSlider) {
        print("\(slider.value)") // e.g., [1.0, 4.5, 5.0]
    }
    
    @IBAction func actnKmSlider(_ sender: Any) {
    }
    
   
}
