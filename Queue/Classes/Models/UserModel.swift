//
//  UserModel.swift
//  Queue
//
//  Created by Vivek Padaya on 06/12/18.
//  Copyright © 2018 Vivek Padaya. All rights reserved.
//

import UIKit

class UserModel: NSObject, Codable {
    var userId : Int = 0
    
    //
    var firstName : String = ""
    var lastName : String = ""
    var emailId : String?
    var mobNo : String?
    var countryCode : String = ""
    
    var passWord : String = ""
    
    var birthDay :String = ""
    var gender :String = ""
    var height :String = ""
    
    var qualification :String = ""
    var institute :String = ""
    
    var profession :String = ""
    var company :String = ""
    

    init(dict : NSDictionary) {
        
        
        if let fname_temp = dict["FirstName"] as? String {
            self.firstName = fname_temp
        }
        
        if let lname_temp = dict["LastName"] as? String {
            self.lastName = lname_temp
        }
        
        if let email_temp = dict["EmailID"] as? String {
            self.emailId = email_temp
        }
        
        if let mob_temp = dict["Mobile"] as? String {
            self.mobNo = mob_temp
        }
        
        if let pwd = dict["Password"] as? String {
            self.passWord = pwd
        }
        
        if let birthday = dict["Birthday"] as? String {
            self.birthDay = birthday
        }
        
        if let gender = dict["Gender"] as? String {
            self.gender = gender
        }
        
        if let height = dict["Height"] as? String {
            self.height = height
        }
    }
    
    override init() {
        super.init()
        print("init")
    }
}
